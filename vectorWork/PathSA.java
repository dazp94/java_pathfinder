package vectorWork;

import java.util.ArrayList;
import java.util.Collections;

import nodeWork.Node;
import nodeWork.Path;
import vectorWork.saWork.SimulatedAnnealing;
import vectorWork.saWork.NodeKeeper;

public class PathSA extends Path{

	@SuppressWarnings("rawtypes")
	private ArrayList path = new ArrayList<Node>();
	private double distance = 0;
	private String sysout;

	@SuppressWarnings("unchecked")
	public PathSA(Node originNode) {
		super(originNode);
		for (int i = 0; i < NodeKeeper.numberOfNodes(); i++) {
			path.add(null);
		}
	}

	// Constructs a path from another path
	@SuppressWarnings("rawtypes")
	public PathSA(Node originNode, ArrayList path){
		super(originNode);
		this.path = (ArrayList) path.clone();
	}


	@Override
	public void calculateBestPath() {
		SimulatedAnnealing sa = new SimulatedAnnealing(this);
		sa.simulatedAnnealingMethod();
	}

	@SuppressWarnings("rawtypes")
	public ArrayList getPathSA(){
		return path;
	}

	// Cria um  caminho aleat�rio organizado
	public void generateIndividual() {
		for (int NodeIndex = 0; NodeIndex < NodeKeeper.numberOfNodes(); NodeIndex++) {
			setNode(NodeIndex, NodeKeeper.getNode(NodeIndex));
		}
		Collections.shuffle(path);
		sysout = sysout + "Shuffled path: " + path;

		//organizar de modo a ter um path a come�ar e terminar num originNode
		organizeShuffledPath();
		sysout = sysout + organizeShuffledPath() + "\n" + "Organized path: " + path;

	}

	public String organizeShuffledPath(){
		boolean first = true;
		sysout = sysout + "Finding the first originNode";
		
		for (int i = 0; i < path.size(); i++) {
			sysout = sysout + "\n" + "...";
			System.out.println("...");
			if(((Node) path.get(i)).getNodeId().toString().equals(this.originNode.getNodeId()) && first == true){
				Collections.swap(path, 0, i);
				sysout = sysout + "Found originNode" + "\n" +
			    "Swapped: " + path.get(0).toString() + " with " + path.get(i).toString();
				first = false;
			}
		}
		
		sysout = sysout + "\n" + "Finding the finishing originNode";

		for (int i = path.size() - 1; i > 0 ; i--) {
			if(((Node) path.get(i)).getNodeId().toString().equals(this.originNode.getNodeId())){
				sysout = sysout + "\n" + "...";
				System.out.println("...");
				Collections.swap(path, path.size() - 1, i);
				sysout = sysout + "" + "Found the finishing originNode" + "\n"
				+ "Swapped: " + path.get(path.size()-1).toString() + " with " + path.get(i).toString();
			}
		}
		sysout = sysout + "Shuffled path is now organized";
		
		return sysout;
	}
	
	public Node getNode(int pathPosition) {
		return (Node)path.get(pathPosition);
	}

	@SuppressWarnings("unchecked")
	public void setNode(int pathPosition, Node Node) {
		path.set(pathPosition, Node);
		
		distance = 0;
	}
	
	//Devolve a dist�ncia do percurso
	public double getDistance(){
		if (distance == 0) {
			double pathDistance = 0;
			for (int NodeIndex=0; NodeIndex < pathSize(); NodeIndex++) {
				Node fromNode = getNode(NodeIndex);
				Node destinationNode;
				
				if(NodeIndex+1 < pathSize()){
					destinationNode = getNode(NodeIndex+1);
				}
				else{
					destinationNode = getNode(0);
				}
				pathDistance = pathDistance + fromNode.distanceTo(destinationNode);
			}
			distance = pathDistance;
		}
		return distance;
	}

	public int pathSize() {
		return path.size();
	}

	@Override
	public String toString() {
		String geneString = "You've ran Stimulated Annealing algorithm" + "\n";
		geneString += "The solution for this path is given by the following list: " + "\n" + "\n";
		
		for (int i = 0; i < path.size(); i++) {
			geneString += path.get(i) + "\n";
		}
		return geneString;
	}

	public String getSysout() {
		return sysout;
	}

}
