package vectorWork;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import nodeWork.Node;
import nodeWork.Path;

public class PathBF extends Path {

	private Path[] paths = new Path[10000];
	private List<String> pathStrings = new ArrayList<String>();
	private double bestDistance = 1111111111;
	private int sizeWithoutFirstNode;
	private int numberOfpermutations = 0;
	private String otherAlternativesCalculated = "";
	private String toPrint = "";

	public PathBF(Node originNode) {
		super(originNode);
	}

	public void calculateBestPath() {

		String pathtoPerm = "";
		/* Cria��o de uma String com todos os ids de Nodes */
		Set<Entry<String, Node>> set = getNodes().entrySet();
		for (Entry<String, Node> me : set) {
			pathtoPerm = pathtoPerm + me.getKey();
			System.out.println(pathtoPerm);
		}

		permutation(pathtoPerm);
		System.out.println(pathStrings);

		for (int i = 0; i < pathStrings.size(); i++) {
			generateNewPath(pathStrings.get(i), i);
		}

		for (int a = 0; a < paths.length; a++) {
			if (paths[a] != null) {

				calculateTotalDistance(paths[a]);

			} else {
				calculatePathPermutations();
				gui.output.setText("Alternatives found during the process:" + "\n" + "---------------------------------------------------" + "\n" +
				otherAlternativesCalculated + "\n" + toPrint + "\n" + "All of " + numberOfpermutations
				+ " possible paths calculated" + "\n" + "The best distance is: " + bestDistance);
				break;
			}
		}
	}

	private void calculateTotalDistance(Path caminho) {
		double pathDistance = 0;
		String output = "Costs between nodes:" + "\n" + "---------------------------------------------------" + "\n" ;
		String bfPath = originNode.getNodeId() + "->";

		for (int i = 0; i <= caminho.getPath().size() - 1; i++) {

			// �ltima itera��o
			if (i == caminho.getPath().size() - 1) {
				output = output + "\n" + "Final analysis: " + "\n" + "---------------------------------------------------" 
			    + "\n" + "Best solution: " + bfPath;
				toPrint = output;
			}

			if (i != caminho.getPath().size() - 1) {
				pathDistance = pathDistance
						+ distanceBetweenNodes(caminho.getPath().get(i), caminho.getPath().get(i + 1));
				String t = "The distance between: " + caminho.getPath().get(i).getNodeId() + " and "
						+ caminho.getPath().get(i + 1).getNodeId() + " is "
						+ distanceBetweenNodes(caminho.getPath().get(i), caminho.getPath().get(i + 1));
				if (i != caminho.getPath().size() - 2) {
					output = output + t + "\n";
					bfPath = bfPath + caminho.getPath().get(i + 1).getNodeId() + "->";
				} else {
					output = output + t + "\n";
					bfPath = bfPath + caminho.getPath().get(i + 1).getNodeId();
				}
			}
		}
		
		if (pathDistance <= bestDistance) {
			setBestDistance(pathDistance);
			otherAlternativesCalculated = otherAlternativesCalculated + "Alternative found: " + bestDistance + "\n";
		}
	}

	// PathWork
	public void setBestDistance(double bestDistance) {
		this.bestDistance = bestDistance;
	}

	private void generateNewPath(String string, int a) {

		char[] charArray = string.toCharArray();
		sizeWithoutFirstNode = charArray.length;
		paths[a] = new PathBF(originNode);
		paths[a].addNodes(originNode);
		paths[a].addToPath(originNode);

		HashMap<String, Node> mapa = getC().getNodes();
		for (int i = 0; i < charArray.length; i++) {
			Set<Entry<String, Node>> set = mapa.entrySet();
			for (Entry<String, Node> me : set) {
				if (charArray[i] == me.getKey().charAt(0)) {
					paths[a].addNodes(me.getValue());
					paths[a].addToPath(me.getValue());
				}
			}
		}
		paths[a].addToPath(originNode);
		/* teste para verificar o path gerado */
		System.out.println(paths[a].getPath().toString());
	}

	// Permutation
	public void permutation(String str) {
		permutation("", str);
	}

	public void permutation(String prefix, String str) {

		int n = str.length();

		if (n == 1)
			pathStrings.add(prefix + str);
		else {
			for (int i = 0; i < n; i++) {
				permutation(prefix + str.charAt(i), str.substring(0, i) + str.substring(i + 1, n));
			}
		}
	}

	public void calculatePathPermutations() {
		numberOfpermutations = sizeWithoutFirstNode;

		for (int i = numberOfpermutations - 1; i > 0; i--) {
			numberOfpermutations = numberOfpermutations * i;
		}
	}
	
	
}
