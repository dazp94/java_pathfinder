package vectorWork.hungWork;


import java.util.Comparator;

public class HungarianPenaltyComparator implements Comparator<HungarianPenalty> {

	@Override
	public int compare(HungarianPenalty o1, HungarianPenalty o2) {
		// TODO Auto-generated method stub
		if(o1.getPenalty() < o2.getPenalty()){
			return 1;
		}
		
		if(o1.getPenalty() > o2.getPenalty()){
			return -1;
		}
		else{
			return 0;
		}
	}
}
