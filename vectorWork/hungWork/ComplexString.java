package vectorWork.hungWork;

public class ComplexString {

	private String from;
	private String to;
	
	public ComplexString(String from, String to){
		this.from = from;
		this.to = to;
	}

	public String getFrom() {
		return from;
	}

	public String getTo() {
		return to;
	}
	
	
}
