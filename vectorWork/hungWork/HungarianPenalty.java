package vectorWork.hungWork;



public class HungarianPenalty{

	private String IcoordinateLetter;
	private String JcoordinateLetter;
	private int i;
	private int j;
	private double penalty;
	
	public HungarianPenalty(String  IcoordinateLetter, String  JcoordinateLetter, double penalty){
		this.IcoordinateLetter = IcoordinateLetter;
		this.JcoordinateLetter = JcoordinateLetter;
		this.penalty=penalty;
	}
	
	public double getPenalty(){
		return penalty;
	}
	
	public String getIcoordinateLetter(){
		return  IcoordinateLetter;
	}
	
	public String getJcoordinateLetter(){
		return JcoordinateLetter;
	}

	public int getI() {
		return i;
	}

	public int getJ() {
		return j;
	}

	public void setI(int i) {
		this.i = i;
	}

	public void setJ(int j) {
		this.j = j;
	}

	public String getPath() {
		// TODO Auto-generated method stub
		return "" + IcoordinateLetter + JcoordinateLetter;
	}

}
