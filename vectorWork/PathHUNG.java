package vectorWork;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import nodeWork.Node;
import nodeWork.Path;
import vectorWork.hungWork.ComplexString;
import vectorWork.hungWork.HungarianPenalty;
import vectorWork.hungWork.HungarianPenaltyComparator;

public class PathHUNG extends Path {

	private String[] matrixHeader;
	private String[][] matrix;
	private int size;
	private int counter = 1;
	private Set<Entry<String, Node>> set;
	private HashMap<Integer, Double> lineAttenuator = new HashMap<Integer, Double>();
	private HashMap<Integer, Double> columnAttenuator = new HashMap<Integer, Double>();
	private List<HungarianPenalty> zeroCells = new LinkedList<HungarianPenalty>();
	private ArrayList<ComplexString> path = new ArrayList<ComplexString>();
	private String solution = originNode.getNodeId();
	private String solutionNotOrganize = "";
	private String output = " " ;
	private int organizeCounter = 1;

	public PathHUNG(Node originNode) {
		super(originNode);
	}

	public void calculateBestPath() {
		this.set = nodes.entrySet();
		this.size = this.set.size();
//		System.out.println("Number of Nodes: " + size);
		createMatrix();
		matrixMinimization();
		printMatrix();
		lastStep();
	}

	private void printMatrix() {
		output = output + "" + "\n";
	
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
//				System.out.print(matrix[i][j] + " ");
				output = output + matrix[i][j] + " ";
			}
//			System.out.println();
			output = output + "" + "\n";
		}
	}

	private void lastStep() {
		penaltyAnalyser();
	}

	private void penaltyAnalyser() {

		verifyZeros();

		double contestantCellPenalty = 0;

//		System.out.println("Verifying all penalties");
		output = output + "Verifying all penalties ..." + "\n";
		// find all zeros and see whose penalty is the highest
		for (int i = 1; i < matrix.length; i++) {
			for (int j = 1; j < matrix.length; j++) {

				String celula = matrix[i][j];

				if (celula.equals("0.0")) {

					// System.out.println();
					// System.out.println("New zero found");
					//String coordinates = String.valueOf(i) + "." + String.valueOf(j);
					output = output + "\n" + "New zero found at: " +  String.valueOf(i) + "." + String.valueOf(j);
					
					// System.out.println("Coordinates: " + coordinates);
					// System.out.println();

					contestantCellPenalty = calculateCellPenalty(i, j);

					HungarianPenalty hungarianPenalty = new HungarianPenalty(matrix[i][0], matrix[0][j],
							contestantCellPenalty);

					hungarianPenalty.setI(i);
					hungarianPenalty.setJ(j);

					zeroCells.add(hungarianPenalty);
//					System.out.println("Adding  " + hungarianPenalty.getPath() + " with penalty " + contestantCellPenalty);
					output = output + "\n" + "Adding" + hungarianPenalty.getPath() + "'s penalty: " + contestantCellPenalty + "\n";
				}
			}
		}

		Collections.sort(zeroCells, new HungarianPenaltyComparator());

		System.out.println();

		if (zeroCells.size() != 0) {
			int symmetricI = 0;
			int symmetricJ = 0;

			symmetricI = zeroCells.get(0).getJ();
			symmetricJ = zeroCells.get(0).getI();

			HungarianPenalty highestPenalty = zeroCells.get(0);

//			System.out.println();
//			System.out.println("I coordinate with highest penalty is: " + highestPenalty.getIcoordinateLetter());
			output = output + "\n" + "I coordinate with highest penalty is: " + highestPenalty.getIcoordinateLetter();
//			System.out.println("J coordinate with highest penalty is: " + highestPenalty.getJcoordinateLetter());
			output = output + "\n" + "J coordinate with highest penalty is: " + highestPenalty.getJcoordinateLetter() + "\n";
//			System.out.println();
//			System.out.println("Now applying NAs because of: " + matrix[highestPenalty.getI()][0]
//					+ matrix[0][highestPenalty.getJ()]);
//			System.out.println();

			for (int i = 1; i < matrix.length; i++) {
				for (int j = 1; j < matrix.length; j++) {
					if (i == highestPenalty.getI() || j == highestPenalty.getJ()) {
						matrix[i][j] = "NA";
					}
				}
			}

//			System.out.println(
//					"Also applied to it's symmetric coordinates: " + matrix[symmetricI][0] + matrix[0][symmetricJ]);
			matrix[symmetricI][symmetricJ] = "NA";
			
			ComplexString cs = new ComplexString(matrix[highestPenalty.getI()][0], matrix[0][highestPenalty.getJ()]);

			solutionNotOrganize = solutionNotOrganize + "->" + matrix[highestPenalty.getI()][0]
					+ matrix[0][highestPenalty.getJ()];
			path.add(cs);
//			System.out.println();
//			System.out.println("---------------------");
//			System.out.println("Matrix modified ");
			output = output + "\n" + "Matrix modified " + "\n";
//			System.out.println("---------------------");
//			System.out.println();
			printMatrix();
			
			// System.out.println("Removing: " + zeroCells.get(0).getPath());
			zeroCells.remove(0);

			// try {
			// Thread.sleep(2000);
			// } catch (InterruptedException e) {
			// e.printStackTrace();
			// }
		}

		if (zeroCells.size() != 0) {
//			System.out.println();
//			System.out.println("Running penaltyAnalyser() again:");
			output = output + "\n" + "Running penaltyAnalyser() again:" + "\n";
			// System.out.println("The path so far is: " + path);
			zeroCells.clear();
			penaltyAnalyser();
		}

		else {
//			System.out.println();
//			System.out.println("---------------------------------------------------------------");
//			System.out.println("No more modifications possible ! The final matrix is ");
			output = output + "\n" +  "No more modifications possible !" + "\n" + "The final matrix is: " + "\n";
//			System.out.println("---------------------------------------------------------------");
//			System.out.println();
			printMatrix();
//			System.out.println();
//			System.out.println("The best path is ");
			organizePath(originNode.getNodeId());
			output = output + solution;
//			System.out.println(solutionNotOrganize);
//			System.out.println(solution);
			calculateDistance();
			gui.output.setText(output);
		}
	}

	private int calculateDistance() {
		int distance = 0;
		// TODO Auto-generated method stub
		return distance ;
	}

	private void organizePath(String a) {
		String nextString = "";

		if (organizeCounter < matrix.length) {
			for (int i = 0; i < path.size(); i++) {
				if (path.get(i).getFrom() == a) {
//					System.out.println("I'm adding " + path.get(i).getTo() + " to the path");
					solution = solution + "->" + path.get(i).getTo();
					nextString = path.get(i).getTo();
					organizeCounter++;
					organizePath(nextString);
				}
			}
		} else {
//			System.out.println("Path is organized !");
			output = output + "\n" + "Path is organized !" + "\n" + "The best path is: " + "\n";
		}

	}

	private void verifyZeros() {
		
//		System.out.println("Verifying if all lines and columns contain atleast one zero");
		output = output + "\n" + "Verifying if all lines and columns contain atleast one zero ..." + "\n";
		for (int i = 1; i < matrix.length; i++) {
			needsLineReductionWork(i);
			for (int j = 1; j < matrix.length; j++) {
				needsColumnReductionWork(j);
			}
		}
	}

	private void needsColumnReductionWork(int b) {

		int cells = 0;
		int NAs = 0;

		for (int i = 1; i < matrix.length; i++) {
			if (!matrix[i][b].equals("0.0")) {
				cells++;
				if (matrix[i][b].equals("NA")) {
					NAs++;
				}
			}
		}
		if (cells == matrix.length - 1) {
			if (NAs != matrix.length - 1) {
//				System.out.println("Running column attenuation at column: " + b);
				specificColumnAttenuator(b);
			}
		}
	}

	private void needsLineReductionWork(int a) {

		int cells = 0;
		int NAs = 0;
		for (int j = 1; j < matrix.length; j++) {
			if (!matrix[a][j].equals("0.0")) {
				cells++;
				if (matrix[a][j].equals("NA")) {
					NAs++;
				}
			}
		}
		if (cells == matrix.length - 1) {
			if (NAs != matrix.length - 1) {
				specificLineAttenuator(a);
			}
		}
	}

	private void specificColumnAttenuator(int b) {
//		System.out.println();
//		System.out.println("Running specificColumnAttenuator at column: " + b);
		double minimumPenalty = 10000000;

		for (int i = 1; i < matrix.length; i++) {
			String celula = matrix[i][b];
			if (celula != "NA") {

				double contestant = Double.parseDouble(celula);

				if (contestant < minimumPenalty) {
					minimumPenalty = contestant;
				}
			}
		}
		for (int i = 1; i < matrix.length; i++) {
			String celula = matrix[i][b];

			if (celula != "NA") {
				double value = Double.parseDouble(celula);
//				System.out.println("This value was: " + value);
				// System.out.println("Valor de atenua��o: " +
				// lineAttenuator.get(i));
				value = value - minimumPenalty;
				matrix[i][b] = String.valueOf(value);
//				System.out.println("Now this value is: " + matrix[i][b]);
			}
		}
	
		printMatrix();
	}

	private void specificLineAttenuator(int a) {
	
//		System.out.println();
//		System.out.println("Running specificLineAttenuator at line: " + a);
		double minimumPenalty = 10000000;
		String[] lineToScan = matrix[a];

		for (int j = 1; j < lineToScan.length; j++) {
			if (lineToScan[j] != "NA") {
				double contestant = Double.parseDouble(lineToScan[j]);
				if (contestant < minimumPenalty) {
					minimumPenalty = Double.parseDouble(lineToScan[j]);
				}
			}
		}
		for (int j = 1; j < matrix.length; j++) {
			String celula = matrix[a][j];

			if (celula != "NA") {
				double value = Double.parseDouble(celula);
//				System.out.println("This value was: " + value);
				// System.out.println("Valor de atenua��o: " +
				// lineAttenuator.get(i));
				value = value - minimumPenalty;
				matrix[a][j] = String.valueOf(value);
//				System.out.println("Now this value is: " + matrix[a][j]);
			}
		}
	
//		System.out.println();
//		System.out.println("--------------------------");
//		System.out.println("Matrix is: ");
//		System.out.println();
		printMatrix();
	}

	private double calculateCellPenalty(int i, int j) {
		double penaltyCost = 0;

		penaltyCost = findLowestValueOnLine(i, j) + findLowestValueOnColumn(i, j);

		return penaltyCost;
	}

	private double findLowestValueOnColumn(int a, int b) {
		double minimumPenalty = 10000000;

		for (int i = 1; i < matrix.length; i++) {
			String celula = matrix[i][b];
			if (celula != "NA") {
				if (i != a) {
					double contestant = Double.parseDouble(celula);

					if (contestant < minimumPenalty) {
						minimumPenalty = contestant;
					}
				}
			}
		}
		// System.out.println("Column Minimum Penalty: " + minimumPenalty);

		return minimumPenalty;
	}

	private double findLowestValueOnLine(int a, int b) {

		double minimumPenalty = 10000000;
		String[] lineToScan = matrix[a];

		for (int j = 1; j < lineToScan.length; j++) {
			if (lineToScan[j] != "NA") {
				if (j != b) {

					double contestant = Double.parseDouble(lineToScan[j]);
					if (contestant < minimumPenalty) {
						minimumPenalty = Double.parseDouble(lineToScan[j]);
					}
				}
			}
		}
		// System.out.println("Line Minimum Penalty: " + minimumPenalty);
		return minimumPenalty;
	}

	private void matrixMinimization() {
		lineMinimizationMatrix();
		columnMinimizationMatrix();
	}

	private void columnMinimizationMatrix() {
		columnAttenuator();
		columnReducer();
	}

	private void lineMinimizationMatrix() {
		lineAttenuator();
		lineReducer();
	}

	private void columnAttenuator() {
		for (int j = 1; j < matrix.length; j++) {
			double a = 0;
			if (j == 1) {
				a = Double.parseDouble(matrix[1][2]);
			} else {
				a = Double.parseDouble(matrix[1][j]);
			}
			for (int i = 1; i < matrix.length; i++) {
				String celula = matrix[i][j];
				if (celula.equals("NA")) {
					// System.out.println("'NA' Found");
				} else {
					double b = Double.parseDouble(celula);

					if (b < a) {
						a = b;
					}
				}
			}
			columnAttenuator.put(j, a);
		}
	}

	private void columnReducer() {

		for (int j = 1; j < matrix.length; j++) {
			for (int i = 1; i < matrix.length; i++) {
				String celula = matrix[i][j];

				if (!celula.equals("NA")) {
					double value = Double.parseDouble(celula);
					value = value - columnAttenuator.get(j);
					matrix[i][j] = String.valueOf(value);
				}
			}
		}
//		System.out.println("-----------------------------------------------");
//		System.out.println("Printing matrix after column reduction:");
		printMatrix();
//		System.out.println("-----------------------------------------------");
//		System.out.println();
	}

	private void lineAttenuator() {

		for (int i = 1; i < matrix.length; i++) {
			double a = 0;
			if (i == 1) {
				a = Double.parseDouble(matrix[2][1]); // o primeiro String[][]
			} else {
				a = Double.parseDouble(matrix[i][1]); // todos os outros casos
			}

			for (int j = 1; j < matrix.length; j++) {

				String celula = matrix[i][j];
				if (celula == "NA") {
					// System.out.println("'NA' Found");
				} else {
					double b = Double.parseDouble(celula);
					if (b < a) {
						a = b;
					}
				}
			}
			lineAttenuator.put(i, a);
		}
//		System.out.println();
	}

	private void lineReducer() {

		for (int i = 1; i < matrix.length; i++) {
			for (int j = 1; j < matrix.length; j++) {
				String celula = matrix[i][j];

				// System.out.println("Linha: " + i);
				if (celula != "NA") {
					double value = Double.parseDouble(celula);
					// System.out.println("Valor de atenua��o: " +
					// lineAttenuator.get(i));
					value = value - lineAttenuator.get(i);
					matrix[i][j] = String.valueOf(value);
				}
			}
		}
//		System.out.println("-----------------------------------------------");
//		System.out.println("Printing matrix after line reduction:");
		printMatrix();
//		System.out.println("-----------------------------------------------");
//		System.out.println("");
	}

	private void createMatrix() {
		generateMatrixHeader();
		generateMatrixRows();
	}

	private void generateMatrixHeader() {
		this.matrixHeader = new String[size + 1];
		matrixHeader[0] = "/|";

		for (Entry<String, Node> me : set) {
//			System.out.println("Counter value: " + counter + " and Node: " + me.getKey());
			matrixHeader[counter] = me.getKey();
			// System.out.println(me.getKey());
			this.counter++;
		}

		matrix = new String[counter][counter];
		matrix[0] = matrixHeader;

//		System.out.println();
//		System.out.println("Matrix:");
//		System.out.println(Arrays.toString(matrixHeader));

	}

	private void generateMatrixRows() {

		String[] row;
		List<Node> values = new ArrayList<Node>(nodes.values());
		Node n = null;

		int i = 1;
		int y = 0;
		while (i < counter) {
			for (int z = 0; z < values.size(); z++) {
				i = 1;
				n = values.get(z);
				row = new String[size + 1]; // +1 para receber a Row Label
				if (z == y) {
					row[0] = values.get(z).getNodeId(); // Row Label
					y++;
				}
				for (int j = 1; j < matrixHeader.length; j++) {
					if (n.getNodeId() == matrixHeader[j]) {
						row[i] = "NA";
						i++;
					} else {
						for (Entry<String, Node> me : set) {
							if (matrixHeader[j] == me.getKey()) {
								row[i] = String.valueOf(distanceBetweenNodes(n, me.getValue()));
								i++;
							}
						}
					}
				}
//				System.out.println(Arrays.toString(row));
				matrix[z + 1] = row;
			}
		}
	}
}
