package vectorWork;

import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.Set;

import nodeWork.Node;
import nodeWork.Path;

public class PathNN extends Path {

	private ArrayList<Node> nnPath = new ArrayList<Node>();
	private Set<Entry<String, Node>> set = getNodes().entrySet();

	public PathNN(Node originNode) {
		super(originNode);
	}

	public void calculateBestPath() {

		nnPath.add(originNode);
		findClosestNode(originNode);
		nnPath.add(originNode);
		calculatePathDistance();
		
	}

	private void calculatePathDistance() {

		for (int i = 0; i < nnPath.size() - 1; i++) {
			pathDistance = pathDistance + nnPath.get(i).distanceTo(nnPath.get(i + 1));
		}

		System.out.println("PathDistance is: " + pathDistance);
		gui.output.setText("" + this);

	}

	private void findClosestNode(Node n) {

		Node b = null;
		double distance = Double.MAX_VALUE;
		if(!set.isEmpty()){
			for (Entry<String, Node> me : set) {
				if (distance > me.getValue().distanceTo(n)){
					distance = me.getValue().distanceTo(n);
					b = me.getValue();
				}
			}
			getNodes().remove(b.getNodeId());
			nnPath.add(b);		
			findClosestNode(b);
		}
	}

	public String toString() {
		String geneString = "You've ran Nearest Neighbour algorithm" + "\n";
		geneString += "The solution for this path is given by the following list: " + "\n";
		geneString += "This path has a distance of: " + pathDistance + "\n" + "\n";

		for (int i = 0; i < nnPath.size(); i++) {
			geneString += nnPath.get(i) + "\n";
		}
		return geneString;
	}

}
