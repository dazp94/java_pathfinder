package vectorWork.saWork;

import java.util.ArrayList;

import nodeWork.Node;

public class NodeKeeper {

    
    @SuppressWarnings("rawtypes")
	public static ArrayList destinationNodes = new ArrayList<Node>();
    private static NodeKeeper nk = new NodeKeeper();
    
    //Singleton
    public static NodeKeeper getNk() {
		return nk;
	}

    @SuppressWarnings("unchecked")
	public static void addNode(Node n) {
        destinationNodes.add(n);
    }
    
    public static Node getNode(int index){
        return (Node) destinationNodes.get(index);
    }
    
    public static int numberOfNodes(){
        return destinationNodes.size();
    }
    
    @SuppressWarnings("rawtypes")
	public static ArrayList getTMList(){
    	return destinationNodes;
    }
}