package vectorWork.saWork;

import gui.TSP;
import nodeWork.Node;
import vectorWork.PathSA;

public class SimulatedAnnealing {

	private Node originNode;
	private TSP gui;
	
	public SimulatedAnnealing(PathSA sa) {
		this.originNode = sa.getOriginNode();
		this.gui = sa.getC();
	}

	public void simulatedAnnealingMethod() {
		
		double temperatura = 10000;
		double arrefecimento = 0.005;

		// Solu��o inicial
		PathSA ongoingSolution = new PathSA(originNode);
		ongoingSolution.generateIndividual();
		
		//output
		int tryNumber = 0;
		PathSA output = ongoingSolution;
		
		System.out.println("Initial solution distance: " + ongoingSolution.getDistance());
		
		//Definir a solu��o inicial como a melhor (isto ser� tempor�rio pois ela vai ser alterada)
		PathSA best = new PathSA(originNode, ongoingSolution.getPathSA());

		while (temperatura > 1) {
			PathSA newSolution = new PathSA(originNode, ongoingSolution.getPathSA());

			int tourPos1 = (int) (newSolution.pathSize() * Math.random());
			int tourPos2 = (int) (newSolution.pathSize() * Math.random());

			Node nodeToSwap1 = newSolution.getNode(tourPos1);
			Node nodeToSwap2 = newSolution.getNode(tourPos2);

			newSolution.setNode(tourPos2, nodeToSwap1);
			newSolution.setNode(tourPos1, nodeToSwap2);
			
			System.out.println("Running organizeShuffledPath: " + tryNumber);
			tryNumber++;
			newSolution.organizeShuffledPath();
			
			int currentEnergy = (int) ongoingSolution.getDistance();
			int neighbourEnergy = (int) newSolution.getDistance();

			if (acceptanceProbability(currentEnergy, neighbourEnergy, temperatura) > Math.random()) {
				ongoingSolution = new PathSA(originNode, newSolution.getPathSA());
			}

			if (ongoingSolution.getDistance() < best.getDistance()) {
				best = new PathSA(originNode, ongoingSolution.getPathSA());
			}

			temperatura = temperatura * (1 - arrefecimento);
		}
	
		gui.output.setText(tryNumber + " number of solutions for a cooling rate of: " +
				"\n" + arrefecimento + "\n" + "\n" + "First solution information: " + "\n" + "\n" + output +
				"\n" + "The first solution had a distance of: " + output.getDistance() + "\n" + "\n" + 
				"***************************************************" + "\n" + "\n" + "Final solution information: " + "\n" + "\n" + best +
				"\n" + "The final solution had a distance of: " + best.getDistance() +
				"\n" + "---------------------------------------------------" + "\n" + "Running this algorithm saved you: " + (output.getDistance() - best.getDistance()) + "\n");
	}

	// Calcular a probabilidade de ser aceite
	public double acceptanceProbability(int energy, int newEnergy, double temperature) {
		
		// Se a nova solu��o for melhor, aceitasse essa mesma solu��o
		if (newEnergy < energy) {
			return 1.0;
		}
		// Caso seja pior calcula-se a probabilidade de ser aceite
		return Math.exp((energy - newEnergy) / temperature);
	}
}