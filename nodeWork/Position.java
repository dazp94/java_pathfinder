package nodeWork;
public class Position {

private double dx, dy;

	public Position(double dx, double dy){
		this.dx = dx;
		this.dy = dy;
}

public double getDx() {
	return dx;
}

public double getDy() {
	return dy;
}
}
