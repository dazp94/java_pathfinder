package nodeWork;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import gui.TSP;

abstract public class Path {

	protected HashMap<String, Node> nodes = new HashMap<String, Node>();//nodes disponiveis, util para procurar por ID
	protected List<Node> path = new ArrayList<Node>();
	protected double pathDistance;
	protected TSP gui;
	protected Node originNode;

	public Path(Node originNode){
		this.originNode=originNode;
	}

	public List<Node> getPath() {
		return path;
	}

	public Node getOriginNode() {
		return originNode;
	}
	
	public TSP getC() {
		return gui;
	}

	public void setGui(TSP gui) {
		this.gui = gui;
	}

	public double distanceBetweenNodes(Node n1, Node n2){
		double x1 = n1.getPosition().getDx();
		double y1 = n1.getPosition().getDy();
		double x2 = n2.getPosition().getDx();
		double y2 = n2.getPosition().getDy();

		double distance = Math.sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));

		return distance;
	}

	public HashMap<String, Node> getNodes() {
		return nodes;
	}

	public void addNodes(Node n) {
		nodes.put(n.getNodeId(), n);
	}

	public void addToPath(Node n){
		path.add(n);
	}
	
	public abstract void calculateBestPath();

}



