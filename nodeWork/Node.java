package nodeWork;

import nodeWork.Position;

public class Node implements Comparable<Node>{

	private String nodeId;
	private Position position;
	private double distance;

	public Node(String nodeId, Position position) {
		this.nodeId = nodeId;
		this.position = position;
	}

	public String getNodeId() {
		return nodeId;
	}

	public Position getPosition() {
		return position;
	}

	public void setDistanceToAnotherNode(double d) {
		distance = d;
	}

	public double getDistance() {
		return distance;
	}

	public String toString() {
		return nodeId + "(" + position.getDx()  + "," + position.getDy() +")";
	}

	public double distanceTo(Node destinationNode) {
		double x1 = this.getPosition().getDx();
		double y1 = this.getPosition().getDy();
		double x2 = destinationNode.getPosition().getDx();
		double y2 = destinationNode.getPosition().getDy();

		double distance = Math.sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));

		return distance;
	}

	@Override
	public int compareTo(Node o) {
		        if (this.getDistance() < o.getDistance()) {
		            return -1;
		        }
		        if (this.getDistance() > o.getDistance()) {
		            return 1;
		        }
		        return 0;
		    }
}
