# tsi_pathfinder
Bem-vindo ao repositório do meu projecto da unidade curricular de Tecnologias para Sistemas Inteligentes lecionada no ISCTE-IUL pelo Prof. Luís Botelho. 
#
Este projecto foi orientado pelo Prof. Ricardo Ribeiro, tendo como intuito apresentar diferentes abordagens para a resolução do problema do Caixeiro-Viajante.
#
A sua utilização assume que o utilizador adapta o diretório presente na classe TSP para o seu diretório de eleição para armazenamento do ficheiro de Nodes.
#
#                                                                                                                                 -Diogo

