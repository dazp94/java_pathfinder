package gui;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;

import nodeWork.Node;
import nodeWork.Path;
import nodeWork.Position;
import vectorWork.PathBF;
import vectorWork.PathHUNG;
import vectorWork.PathNN;
import vectorWork.PathSA;
import vectorWork.saWork.NodeKeeper;

public class TSP extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Node firstNode;
	private Path path;
	private HashMap<String, Node> nodes =  new HashMap<String,Node>();
	public JTextArea output;
	private int j = 0;
	private String fileName = "C:/Users/Diogo/workspace/PathFinder_TSI_Final/src/Nodes.txt";
	private File f = new File(fileName);
	@SuppressWarnings("unused")
	private JTextArea jtextarea_1;
	private NodeKeeper nk = NodeKeeper.getNk();
	//private ArrayList<Node> tm = NodeKeeper.getTMList();
	
	public TSP() {
	}

	public void init(){
		generateGui();
		this.setVisible(true);
	}
	private void generateGui() {
		getContentPane().setBackground(new Color(72, 133, 237));

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}

		getContentPane().setLayout(null);

		JPanel algorithmPanel = new JPanel();
		algorithmPanel.setBackground(new Color(72, 133, 237));
		algorithmPanel.setBounds(48, 11, 348, 110);
		getContentPane().add(algorithmPanel);
		algorithmPanel.setLayout(null);

		JLabel lblChooseYourAlgorithm = new JLabel("Choose your algorithm");
		lblChooseYourAlgorithm.setFont(new Font("Roboto", Font.BOLD, 12));
		Color c = new Color(255, 255, 255);
		lblChooseYourAlgorithm.setForeground(c);
		lblChooseYourAlgorithm.setBounds(33, 31, 126, 34);
		algorithmPanel.add(lblChooseYourAlgorithm);

		JButton btnNN = new JButton("Nearest Neighbor");
		btnNN.setBounds(198, 11, 130, 23);
		algorithmPanel.add(btnNN);

		JButton btnBF = new JButton("         Brute Force     ");
		btnBF.setBounds(198, 31, 130, 23);
		algorithmPanel.add(btnBF);

		JButton btnHUNG = new JButton("Hungarian Method");
		btnHUNG.setBounds(198, 53, 130, 23);
		algorithmPanel.add(btnHUNG);
		
		JButton btnSA = new JButton("Simulated Annealing");
		btnSA.setBounds(198, 76, 130, 23);
		algorithmPanel.add(btnSA);

		btnNN.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
			
				j = 1;
				doJob(f);
				nnMethodHandler();
			}
		});

		btnBF.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				j = 2;
				doJob(f);
				bfMethodHandler();
			}
		});

		btnHUNG.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				j = 3;
				doJob(f);
				hungMethodHandler();
			}
		});
		
		btnSA.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				j = 4;
				doJob(f);
				saMethodHandler();
			}
		});

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(7, 132, 430, 317);
		getContentPane().add(scrollPane);
		JTextArea jtextarea_1 = new JTextArea();
		scrollPane.setViewportView(jtextarea_1);
		jtextarea_1.setColumns(10);
		
		this.output=jtextarea_1;
		this.setSize(451, 500);
		centreWindow(this);
		this.setResizable(false);
	}
	
	public static void centreWindow(Window frame) {
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (int) ((dimension.getWidth() - frame.getWidth()) / 3);
		int y = (int) ((dimension.getHeight() - frame.getHeight()) / 4);
		frame.setLocation(x, y);
	}
	
	//Algorithm Handlers (file)
	private void doJob(File f){

		try {
			countnumberOfNodes(f);
			generateAllNodes(f);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	private void countnumberOfNodes(File f) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(f));
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append("(" + line + ")" + "\n");
//				sb.append(System.lineSeparator());
				line = br.readLine();
				//numberOfNodes++;
			}
			String everything = sb.toString();
			System.out.println(everything);
		} finally {
			br.close();
		}
	}

	//Node work
	private void generateAllNodes(File f) throws IOException {
		
		nodes.clear();
		
		Scanner inputFile = new Scanner(f);
		int i = 0;
		while (inputFile.hasNext())
		{
			String nodeInfo = inputFile.nextLine();
			if (i == 0) {
				generateFirstNode(nodeInfo);
				i++;
			} else {
				generateNodes(nodeInfo);
			}
		}
		inputFile.close();
	}
	@SuppressWarnings("static-access")
	private void generateFirstNode(String text) {

		String cols[] = text.split(",");
		String id = cols[0];
		String x = cols[1];
		String y = cols[2];
		
		double dx = Double.parseDouble(x);
		double dy = Double.parseDouble(y);
		firstNode = new Node(id, new Position(dx, dy));
		if(j==1){
			nk.destinationNodes.clear();
			path = new PathNN(firstNode);
			path.setGui(this);
		}

		else if(j==2){
			nk.destinationNodes.clear();
			path = new PathBF(firstNode);
			path.setGui(this);
		}
		else if(j==3){
			nk.destinationNodes.clear();
			path = new PathHUNG(firstNode);
			nodes.put(id, firstNode);//importante para o pathBF
			path.addNodes(firstNode);//importante para o pathNN e pathHUNG
			path.setGui(this);
		}
		else if(j==4){
			nk.destinationNodes.clear();
			path = new PathSA(firstNode);
			nk.addNode(firstNode);
			nk.addNode(firstNode);
			path.setGui(this);
		}
	}
	private void generateNodes(String text) {
		
		String cols[] = text.split(",");
		String id = cols[0];
		String x = cols[1];
		String y = cols[2];

		double dx = Double.parseDouble(x);
		double dy = Double.parseDouble(y);
		Node n = new Node(id, new Position(dx, dy));
		nodes.put(n.getNodeId(), n);//importante para o pathBF
		path.addNodes(n);//importante para o pathNN e pathHUNG
		nk.addNode(n);
}
	
	public HashMap<String, Node> getNodes() {
		return nodes;
	}
	//OutputWriter
	public JTextArea getOutput() {
		return output;
	}
	public void setOutput(String t) {
		output.setText(t);
	}
	private void nnMethodHandler() {
		path.calculateBestPath();
	}	
	private void bfMethodHandler() {
		Set<Entry<String, Node>> set = nodes.entrySet();
		for (Entry<String, Node> me : set) {
			System.out.println(me.getValue().getNodeId());
		}
		path.calculateBestPath();
	}
	private void hungMethodHandler(){
		path.calculateBestPath();
	}
	private void saMethodHandler(){
		path.calculateBestPath();
	}
	
}
